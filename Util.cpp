//
// Created by fifal on 3.1.17.
//

#include "Util.h"

void Util::print_vector(std::vector<FATStruct::cluster_data> clusters) {
    for (unsigned long i = 0; i < clusters.size(); ++i) {
        std::cout << "Cluster " << clusters.at(i).start_cluster << "-" << clusters.at(i).end_cluster << ":\t\t"
                  << clusters.at(i).data << std::endl;

        if (clusters.at(i).clusters.size() > 0) {
            std::cout << "Na kterých je clusterech: " << clusters.at(i).clusters.size() << ":{";
            for (int j = 0; j < clusters.at(i).clusters.size(); ++j) {
                std::cout << clusters.at(i).clusters.at(j) << "; ";
            }
            std::cout << "}" << std::endl;
        }
    }
}

void Util::print_vector(std::vector<FATStruct::directory_data> directories) {
    for (unsigned long i = 0; i < directories.size(); ++i) {
        std::cout << directories.at(i).dir->name << " - is_file: " << directories.at(i).dir->is_file
                  << " - start_cluster: "
                  << directories.at(i).dir->start_cluster << " - nadsložka: " << directories.at(i).super_directory
                  << std::endl;
    }
}

/**
 * Returns vector which contains message splited by delimiter
 * @param str message to split
 * @param delim delimiter
 * @return vector splitted message
 */
std::vector<std::string> Util::split(const std::string str, const std::string delim) {
    {
        std::vector<std::string> tokens;
        size_t prev = 0, pos = 0;
        do {
            pos = str.find(delim, prev);
            if (pos == std::string::npos) pos = str.length();
            std::string token = str.substr(prev, pos - prev);
            if (!token.empty()) tokens.push_back(token);
            prev = pos + delim.length();
        } while (pos < str.length() && prev < str.length());
        return tokens;
    }
}

std::string Util::get_file_content(std::string file_path) {
    std::ifstream is(file_path);
    if (is.fail()) {
        return "";
    }

    std::stringstream buffer;
    buffer << is.rdbuf();
    return buffer.str();
}

std::vector<FATStruct::cluster_data> Util::sort_vector(std::vector<FATStruct::cluster_data> cluster_data) {
    std::vector<FATStruct::cluster_data> sorted_vector = cluster_data;

    for (int i = 0; i < sorted_vector.size() - 1; i++) {
        for (int j = 0; j < sorted_vector.size() - i - 1; j++) {
            if(sorted_vector.at(j).start_cluster > sorted_vector.at(j+1).start_cluster){
                FATStruct::cluster_data temp = sorted_vector.at(j);
                sorted_vector.at(j) = sorted_vector.at(j+1);
                sorted_vector.at(j+1) = temp;
            }
        }
    }

    return sorted_vector;
}

std::vector<FATStruct::directory_data> Util::sort_vector(std::vector<FATStruct::directory_data> directory_data) {
    std::vector<FATStruct::directory_data> sorted_vector = directory_data;

    for (int i = 0; i < sorted_vector.size() - 1; i++) {
        for (int j = 0; j < sorted_vector.size() - i - 1; j++) {
            if(sorted_vector.at(j).super_directory > sorted_vector.at(j+1).super_directory){
                FATStruct::directory_data temp = sorted_vector.at(j);
                sorted_vector.at(j) = sorted_vector.at(j+1);
                sorted_vector.at(j+1) = temp;
            }
        }
    }
    return sorted_vector;
}


