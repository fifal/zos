//
// Created by fifal on 3.1.17.
//

#ifndef ZOS_UTIL_H
#define ZOS_UTIL_H


#include <vector>
#include "FATStruct.h"
#include <iostream>
#include <fstream>
#include <sstream>

class Util {
public:
    static void print_vector(std::vector<FATStruct::cluster_data> clusters);

    static void print_vector(std::vector<FATStruct::directory_data> directories);

    static std::vector<std::string> split(const std::string str, const std::string delim);

    static std::string get_file_content(std::string file_path);

    static std::vector<FATStruct::directory_data> sort_vector(std::vector<FATStruct::directory_data> directory_data);
    static std::vector<FATStruct::cluster_data> sort_vector(std::vector<FATStruct::cluster_data> cluster_data);
};


#endif //ZOS_UTIL_H
