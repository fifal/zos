//
// Created by fifal on 21.12.16.
//

#ifndef ZOS_FAT_H
#define ZOS_FAT_H

#include <cstdint>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <vector>
#include <assert.h>
#include <cmath>
#include "FATStruct.h"
#include "Util.h"

class FAT {
private:
    /************************************************
     *                  Variables                   *
     ***********************************************/
    const int32_t FAT_UNUSED = INT32_MAX - 1;
    const int32_t FAT_FILE_END = INT32_MAX - 2;
    const int32_t FAT_BAD_CLUSTER = INT32_MAX - 3;
    const int32_t FAT_DIRECTORY = INT32_MAX - 4;
    const int32_t FAT_SIMULATED_ERROR = INT32_MAX - 5;

    std::string file_path;

    //------------------- FAT ----------------------
    struct FATStruct::boot_record br;
    std::vector<FATStruct::directory_data> directories;
    std::vector<FATStruct::cluster_data> clusters;
    int32_t **fat_table;

    /**********************************************/


    /************************************************
     *                  Methods                     *
     ***********************************************/
    int parse_data(std::string file_path);
    int load_boot_record(FILE *file);


    int check_tables_for_errors();
    int repair_table(int error);
    int32_t find_correct_value(int32_t fat_table_index);

    //----------- FUNCTIONS -----------

    // #7 - print filesystem
    void print_dir(int indention_level, FATStruct::directory_data dir_data);
    std::vector<FATStruct::directory_data> get_children(int32_t parent);
    int32_t get_file_cluster_count(int32_t start_cluster);
    //---------------------------------

    // #5 - delete empty dir
    //---------------------------------

    // #4 - make new dir
    //---------------------------------

    // #3 - print file clusters
    struct FATStruct::cluster_data get_file_content(int32_t parent, int level, std::string path);
    struct FATStruct::cluster_data find_file_content(FATStruct::directory_data directory_data);
    struct FATStruct::directory_data get_file_by_start_cluster(int32_t start_cluster);
    //---------------------------------

    // #2 - delete file from fs
    void delete_file_clusters_from_fat_table(std::vector<int32_t> clusters);
    //---------------------------------

    // #1 - load file into fs
    struct FATStruct::directory_data find_dir_by_path(std::string dir_path, int32_t parent, int iteration);
    std::vector<int32_t> find_free_consecutive_clusters(int32_t number_of_clusters);
    std::vector<int32_t> find_free_clusters(int32_t number_of_clusters);
    void write_file_clusters_to_fat_table(std::vector<int32_t> clusters);
    //---------------------------------

    /**********************************************/
public:
    /************************************************
     *                  Variables                   *
     ***********************************************/
    /**********************************************/
    /************************************************
     *                  Methods                     *
     ***********************************************/
    FAT(std::string file_path);

    ~FAT();

    //----------- WRITE -----------
    int write_to_file();
    int write_to_file(std::string file_path);

    //----------- FUNCTIONS -----------

    // #7 - print filesystem
    void print_fat();
    //---------------------------------

    // #6 - print file content
    void print_file_content(std::string file_path);
    //---------------------------------

    // #5 - delete empty dir
    void delete_dir(std::string dir_path);
    //---------------------------------

    // #4 - make new dir
    void make_new_dir(std::string dir_name, std::string dir_path);
    //---------------------------------

    // #3 - print file clusters
    void print_file_clusters(std::string fat_file_path);
    //---------------------------------

    // #2 - delete file from fs
    void delete_file_from_fs(std::string fat_file_path);
    //---------------------------------

    // #1 - load file into fs
    void load_file_into_fs(std::string file_path, std::string dir);
    //---------------------------------

    struct FATStruct::boot_record get_boot_record();
    int32_t **get_fat_table();
    std::vector<FATStruct::directory_data> get_directories();
    std::vector<FATStruct::cluster_data> get_clusters();
    /**********************************************/
};


#endif //ZOS_FAT_H
