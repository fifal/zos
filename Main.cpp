#include <iostream>
#include "FAT.h"
#include "Util.h"
#include "FATTest.h"

const int8_t FAT_TYPE = 12;
const int8_t FAT_COPIES = 2;
const int16_t CLUSTER_SIZE = 256; //256
const int32_t USABLE_CLUSTER_COUNT = pow(2, FAT_TYPE) - 5; //251

int main(int argc, char *argv[]) {
    if (argc < 3) {
        std::cout << "NOT ENOUGH ARGUMENTS" << std::endl;
        return 1;
    }

    if (strcmp(argv[2], "-create") != 0 && strcmp(argv[2], "-break") != 0 && strcmp(argv[2], "-repair") != 0) {
        FAT fat(argv[1]);

        // #1 Load file into file system (0: ./program, 1: vaseFAT.dat, 2: -a, 3: s1, 4: ADR)
        if (strcmp(argv[2], "-a") == 0 && argc == 5) {
            fat.load_file_into_fs(argv[3], argv[4]);
            return 0;
        }

        // #2 Delete file from file system (0: ./program, 1: vaseFAT.dat, 2: -f, 3: s1)
        if (strcmp(argv[2], "-f") == 0 && argc == 4) {
            fat.delete_file_from_fs(argv[3]);
            return 0;
        }

        // #3 Print file clusters (0: ./program, 1: vaseFAT.dat, 2: -c, 3: s1)
        if (strcmp(argv[2], "-c") == 0 && argc == 4) {
            fat.print_file_clusters(argv[3]);
            return 0;
        }

        // #4 Make new dir (0: ./program, 1: vaseFAT.dat, 2: -m, 3: ADR, 4: ADR2)
        if (strcmp(argv[2], "-m") == 0 && argc == 5) {
            fat.make_new_dir(argv[3], argv[4]);
            return 0;
        }

        // #5 Remove empty dir (0: ./program, 1: vaseFAT.dat, 2: -r, 3: ADR)
        if (strcmp(argv[2], "-r") == 0 && argc == 4) {
            fat.delete_dir(argv[3]);
            return 0;
        }

        // #6 Print file content (0: ./program, 1: vaseFAT.dat, 2: -l, 3: s1)
        if (strcmp(argv[2], "-l") == 0 && argc == 4) {
            fat.print_file_content(argv[3]);
            return 0;
        }

        // #7 Print file system (0: ./program, 1: vaseFAT.dat, 2: -p)
        if (strcmp(argv[2], "-p") == 0 && argc == 3) {
            fat.print_fat();
            return 0;
        }
    }

    // #8 Create new FAT (0: ./program, 1: nazevFAT.dat, 2: -create)
    if (strcmp(argv[2], "-create") == 0 && argc == 3) {
        FATTest::create_test_fat(argv[1], FAT_TYPE, FAT_COPIES, CLUSTER_SIZE, USABLE_CLUSTER_COUNT);
        return 0;
    }

    // #9 Breaks FAT table
    if (strcmp(argv[2], "-break") == 0 && argc == 3) {
        FAT fat(argv[1]);
        std::cout << "Simuluji chybu do fat_tabulky[0][6] = 5000; - ukazuje mimo souborový systém" << std::endl;
        fat.get_fat_table()[0][6] = 5000;
        std::cout << "Simuluji chybu do fat_tabulky[0][8] = 7; - smyčka (8->7; 7->8)" << std::endl;
        fat.get_fat_table()[0][8] = 7;
        std::cout << "Simuluji chybu do fat_tabulky[0][30] = 31; - ukazuje na prázdný cluster index > current_index" << std::endl;
        fat.get_fat_table()[0][30] = 31;
        std::cout << "Simuluji chybu do fat_tabulky[0][33] = 31; - ukazuje na prázdný cluster index < current_index" << std::endl;
        fat.get_fat_table()[0][33] = 31;
        std::cout << "Simuluji chybu do fat_tabulky[0][35] = 34; (34 = FAT_DIRECTORY) místo FAT_FILE_END index < current_index" << std::endl;
        fat.get_fat_table()[0][35] = 34;
        std::cout << "Simuluji chybu do fat_tabulky[0][29] = 34; (34 = FAT_DIRECTORY) místo FAT_FILE_END index > current_index" << std::endl;
        fat.get_fat_table()[0][15] = 34;
        fat.write_to_file();
    }

    // #10 Repair FAT table
    if (strcmp(argv[2], "-repair") == 0 && argc == 3) {
        FAT fat(argv[1]);
        fat.print_fat();
        fat.write_to_file();
    }

    return 0;
}